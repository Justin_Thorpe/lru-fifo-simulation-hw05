/**
 * Implements the FIFO algorithm within the ReplacementAlgorithm abstract insert method.
 *
 * @author Justin T.
 * @version 4/1/2019
 *
 */

import java.util.*;
import java.io.*;

public class FIFO extends ReplacementAlgorithm
{
   private int[][] memory;
   private LinkedList<Integer> loaded = new LinkedList<Integer>();
   private int refSize;
   private int column = 0;
   
   /**
    * Constructer, initializes the int memory double array as well as ReplacementAlgorithm.
    *
    * @param refSize the page reference amount based upon pageGenerator
    * @param pageFrameCount number of page frames based upon entered values through test.java
    */
   public FIFO(int refSize, int pageFrameCount)
   {
      super(pageFrameCount);
      this.refSize = refSize;
      memory = new int[pageFrameCount][refSize];
      for(int i=0; i<pageFrameCount; i++){
         for(int j=0; j<refSize; j++){
            memory[i][j] = -1;
         }
      }
   }
   
   /**
    * inserts pageNumber into linked list and calls addToMemory()
    *
    * @param pageNumber the page number being inserted
    */
   public void insert(int pageNumber)
   {
      if(loaded.contains(pageNumber)==false){
         pageFaultCount++;
         if(loaded.size()<pageFrameCount){
            loaded.addLast(pageNumber);
            addToMemory();
         }else{
            loaded.remove(0);
            loaded.addLast(pageNumber);
            addToMemory();
         }
      }
   }
   
   /**
    * Adds pageNumber to int[][] memory for visual display
    *
    */
   public void addToMemory()
   {
      
      for(int i=0; i<loaded.size(); i++){
         memory[i][column] = loaded.get(i);
      }
      
      column++;
      
   }
   
   /**
    * displays the int[][] memory
    *
    */
   public void displayMemory()
   {  
      for(int i=0; i<pageFrameCount; i++){
         for(int j=0; j<refSize; j++){
            if(memory[i][j] > -1){
               System.out.print(" "+memory[i][j]);
            }else{
               System.out.print(memory[i][j]);
            }
         }
         System.out.println();
      }
   }
   
   /**
    * getter method for pageFaultCount
    *
    * @return pageFaultCount the number of page faults
    */
   public int getNumPageFaults()
   {
      return pageFaultCount;
   }
}