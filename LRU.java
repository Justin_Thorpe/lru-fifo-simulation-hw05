/**
 * Implements an LRU page fault system.
 * 
 * @author Laci Wright
 * @version 3/29/2019
 * 
 */

import java.util.*;
import java.io.*;
import java.util.concurrent.LinkedBlockingQueue;

class LRU extends ReplacementAlgorithm{

   private int[][] memory;
   private int[] pageRef;
   private int refSize;
   private LinkedBlockingQueue q = new LinkedBlockingQueue(); //keeps track of least recently used
   private int n = 0; //current column number
   private int temp;


   /**
    * Initializes class variables.
    *
    * @param int referenceSize number of references
    * @param int pageFrameCount number of page frames 
    */
   public LRU(int referenceSize, int pageFrameCount){
      super(pageFrameCount);
      refSize = referenceSize;
      memory = new int[super.pageFrameCount][referenceSize];
      fillMemory();
      pageRef = new int[referenceSize];
      temp = rowNum();
   }

   /**
    * Inserts integer value into memory.
    *
    * @param int pageNumber value to be inserted
    */
   public void insert(int pageNumber){
      try{      
         //if col is not full
         if(temp != -1){
            //if pageNumber not in current memory
            if(check(pageNumber) == false){
               super.pageFaultCount++;
               if(temp > -1){
                  for(int i=n; i<refSize; i++){
                     memory[temp][i] = pageNumber;
                  }
                  addToQ(pageNumber);
               }
            }
         //if col is full
         }else{
            //if pageNumber not in current memory
            if(check(pageNumber) == false){
               int replace = (int)q.poll();
               fault(replace, pageNumber);
            }
         }
         n++;
         temp = rowNum();
      }catch(Exception e){}
   }  
   
   /**
    * Displays the memory array.
    *
    */
   public void displayMemory(){
      for(int i=0; i<super.pageFrameCount; i++){
         for(int j=0; j<refSize; j++){
            if(memory[i][j] > -1){
               System.out.print(" "+memory[i][j]);
            }else{
               System.out.print(memory[i][j]);
            }
         }
         System.out.println();
      }
   }
   
   /**
    * Retrieves empty row number for current column.
    *
    * @return i first empty row number
    * @return -1 if the colomn is full
    */
   public int rowNum(){
      for(int i=0; i<super.pageFrameCount; i++){
         //if(mem[currentCol][i] is empty)
         if(memory[i][n] == -1){
            //return empty row val
            return i;
         }
      }
      //return full 
      return -1;
   }
   
   /**
    * Checks to see if the value to be inserted is already in memory.
    *
    * @param int pageNumber value to be inserted
    *
    * @return true if value is already in memory
    * @return false if values is not in memory
    */
   public boolean check(int pageNumber){
      for(int i=0; i<super.pageFrameCount; i++){
         //if pageNumber is already in current column
         if(memory[i][n] == pageNumber){
            return true;
         }
      } 
      return false;
   }
   
   /**
    * Handles page faults when memory is full.
    *
    * @param int replace value to be replaced
    * @param int pageNumber new value to be inserted
    */
   public void fault(int replace, int pageNumber)throws Exception{
      for(int i=0; i<super.pageFrameCount; i++){
         if(memory[i][n] == replace){
            for(int j=n; j<refSize; j++){
               memory[i][j] = pageNumber;
            }
            q.put(pageNumber);
         }
      } 
      super.pageFaultCount++;
   }
   
   /**
    * Adds an integer value to the queue.
    *
    * @param int add value to be inserted
    */
   public void addToQ(int add)throws Exception{
      if(q.contains(add)){
            q.remove(add);
            q.put(add);
         }else{
            q.put(add);
         }
   }
   
   /**
    * Fills the memory array with -1s.
    *
    */
   public void fillMemory(){
      for(int i=0; i<super.pageFrameCount; i++){
         for(int j=0; j<refSize; j++){
            memory[i][j] = -1;
         }
      }
   }
   
   /**
    * A getter method for the number of page faults.
    *
    * @return int value of page faults
    */
   public int getNumPageFaults(){
      return super.pageFaultCount;
   }
}