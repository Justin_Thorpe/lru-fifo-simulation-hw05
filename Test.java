/**
 * Tests the FIFO and LRU classes and displays results.
 * 
 * @author Laci Wright
 * @version 3/29/2019
 * 
 */

import java.io.*;
import java.util.*;

class Test{
   public static void main(String[] args){
      
      int refSize = Integer.parseInt(args[0]);
      int pageFrame = Integer.parseInt(args[1]);
      
      PageGenerator pageGen = new PageGenerator(refSize);
      pageGen.pageFill();
      int[] insert = pageGen.getReferenceString();
      
      System.out.println("Pages: "+Arrays.toString(insert));
      System.out.println();
      
      LRU lru = new LRU(refSize, pageFrame);
      System.out.println("LRU");
      System.out.println("----------------------------------------");
      for(int i=0; i<refSize; i++){
         lru.insert(insert[i]);
      }
      lru.displayMemory();
      System.out.println();
      System.out.println("Number of page faults: "+lru.getNumPageFaults());
   
      System.out.println();
      
      FIFO fifo = new FIFO(refSize, pageFrame);
      System.out.println("FIFO");
      System.out.println("----------------------------------------");
      for(int i=0; i<refSize; i++){
         fifo.insert(insert[i]);
      }
      fifo.displayMemory();
      System.out.println();
      System.out.println("Number of page faults: "+fifo.getNumPageFaults());
   }
}