/**
 * @author from Operating Systems with Java by Silberschatz, Galvin, and Gagne
 * @version 22 Mar 2010
 *
 * Typed by T.Sergeant
*/

public abstract class ReplacementAlgorithm
{
	protected int pageFaultCount;
	protected int pageFrameCount;


	/**
	 * pageFrameCount is the number of physical page frames in the memory
	*/
	public ReplacementAlgorithm(int pageFrameCount)
	{
		if (pageFrameCount < 0)
			throw new IllegalArgumentException();

		this.pageFrameCount= pageFrameCount;
		pageFaultCount= 0;
	}


	/** getter for pageFaultCount */
	public int getPageFaultCount() { return pageFaultCount; }


	/** Performs an insert of the specified page into memory which may result
	 * in a page fault ...
	*/
	public abstract void insert(int pageNumber);

}
