/**
 * Generates an array of page references with page numbers from 0 to 9.
 * 
 * @author Justin T.
 * @version 3/24/2019
 * 
 */

import java.util.*;

public class PageGenerator
{
   private int[] pageReferences;  // array for page references
   private int referenceAMT;      // int for page reference amount, specified by method call
   
   /**
    * Sets the amount of page references to be had, and
    * initializes the array of page references.
    *
    * @param referenceAMT the number of page references
    */
   public PageGenerator(int referenceAMT)
   {
      this.referenceAMT = referenceAMT;
      pageReferences = new int[referenceAMT];
   }
   
   /**
    * Fills the pageRefernce array with random page numbers ranging
    * from 0 to 9.
    *
    * <p> I'm not sure whether these should indeed be random or not. </p>
    */
   public void pageFill()
   {
      Random pageNum = new Random();
      for(int i=0; i<referenceAMT; i++){
         pageReferences[i] = pageNum.nextInt(10);
      }
   }
   
   /**
    * Getter method for the pageReference array.
    *
    * @return pageReference[] when called 
    */
   public int[] getReferenceString()
   {
      return pageReferences;
   }
   
   /**
    * Prints pageNum of pageReferences array.
    *
    * <p> I used this to test the pageFill method. </p>
    */
   public void arrayToString()
   {
      System.out.println(Arrays.toString(pageReferences));
   }
}